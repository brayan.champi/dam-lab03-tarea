import React, {useState} from 'react';
import {View, ScrollView, StyleSheet, Button} from 'react-native';

import AgeValidator from './app/components/ageValidator';
import MyList from './app/components/myList';


export default function App() {

    const [componente, setComponente] = useState('edad');

    const cambiarVista = () => {
        if (componente === 'edad') {
            setComponente('lista');
        } else {
            setComponente('edad');
        }
    };

    return (
        <ScrollView style={styles.container} contentContainerStyle={{justifyContent: 'space-between'}}>
            {componente === 'edad'
                ?
                <View>
                    <Button color='#efb810' onPress={cambiarVista} title="Cambiar a Lista"/>
                    <AgeValidator/>
                </View>
                :
                <View>
                    <Button color='#efb810' onPress={cambiarVista} title="Cambiar a Validator"/>
                    <MyList/>
                </View>
            }

        </ScrollView>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});
