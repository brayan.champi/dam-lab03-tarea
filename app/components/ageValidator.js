import React, {useState} from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native';


function AgeValidator () {

  const [edad, setEdad] = useState("");
  const [mensaje, setMensaje] = useState("");


  function cambiarEdad(tEdad) {
    setEdad(tEdad);
    if(tEdad !== ""){
      if(tEdad>0){
        if(tEdad >= 18){
          setMensaje("Es mayor de edad")
        }else{
          setMensaje("Es menor de edad")
        }
      }else{
        setMensaje("Ingrese un valor valido")
      }
    }else{
      setMensaje("")
    }
  }

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.text}>Ingrese su edad</Text>
            </View>
            <TextInput
                keyboardType="numeric"
                style={styles.input}
                onChangeText={text => cambiarEdad(text)}
                value={edad}
            />
            <Text style={styles.textResult}> {mensaje}</Text>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10,
        height: '100%',
    },
    text: {
        alignItems: 'center',
        textAlign: 'center',
        padding: 10,
        fontSize: 20,
        color: 'blue'
    },
    textResult: {
        alignItems: 'center',
        textAlign: 'center',
        padding: 10,
        fontSize: 20,
        color: 'red'

    },
    input: {
        height: 40,
        borderColor: '#fff',
        borderWidth: 1,
        backgroundColor: '#3CBC8D',
        borderRadius: 10,
        color: 'white',
        paddingLeft: 10,
        marginBottom: 30,
        fontSize: 16,
    },
});

export default AgeValidator;
