import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
} from 'react-native';

const DATA = [
  {
    id: '0F8JIqi4zwvb77FGz6Wt',
    lastName: 'Fiedler',
    firstName: 'Heinz-Georg',
    email: 'heinz-georg.fiedler@example.com',
    title: 'mr',
    picture: 'https://randomuser.me/api/portraits/men/81.jpg',
  },
  {
    id: '0P6E1d4nr0L1ntW8cjGU',
    picture: 'https://randomuser.me/api/portraits/women/74.jpg',
    lastName: 'Hughes',
    email: 'katie.hughes@example.com',
    title: 'miss',
    firstName: 'Katie',
  },
  {
    id: '1Lkk06cOUCkiAsUXFLMN',
    title: 'mr',
    lastName: 'Aasland',
    firstName: 'Vetle',
    picture: 'https://randomuser.me/api/portraits/men/97.jpg',
    email: 'vetle.aasland@example.com',
  },
  {
    id: '1OuR3CWOEsfISTpFxsG7',
    picture: 'https://randomuser.me/api/portraits/men/66.jpg',
    lastName: 'Vasquez',
    email: 'dylan.vasquez@example.com',
    title: 'mr',
    firstName: 'Dylan',
  },
  {
    id: '1pRsh5nXDIH3pjEOZ17A',
    lastName: 'Vicente',
    title: 'miss',
    firstName: 'Margarita',
    email: 'margarita.vicente@example.com',
    picture: 'https://randomuser.me/api/portraits/women/5.jpg',
  },
  {
    id: '3JAf8R85oIlxXd58Piqk',
    email: 'joey.oliver@example.com',
    title: 'mr',
    firstName: 'Joey',
    lastName: 'Oliver',
    picture: 'https://randomuser.me/api/portraits/men/61.jpg',
  },
  {
    id: '5aZRSdkcBOM6j3lkWEoP',
    picture: 'https://randomuser.me/api/portraits/women/50.jpg',
    email: 'lilja.lampinen@example.com',
    lastName: 'Lampinen',
    firstName: 'Lilja',
    title: 'ms',
  },
  {
    id: '5tVxgsqPCjv2Ul5Rc7gw',
    email: 'abigail.liu@example.com',
    lastName: 'Liu',
    title: 'miss',
    picture: 'https://randomuser.me/api/portraits/women/83.jpg',
    firstName: 'Abigail',
  },
  {
    id: '6wy6UNkZueJfIUfq88d5',
    picture: 'https://randomuser.me/api/portraits/women/32.jpg',
    firstName: 'Melanie',
    email: 'melanie.pilz@example.com',
    title: 'miss',
    lastName: 'Pilz',
  },
  {
    id: '7DbXNPWlNDR4QYVvFZjr',
    email: 'evan.carlson@example.com',
    firstName: 'Evan',
    picture: 'https://randomuser.me/api/portraits/men/80.jpg',
    lastName: 'Carlson',
    title: 'mr',
  },
  {
    id: '8RQd4OVqvmV0I4UlWETQ',
    email: 'kitty.steward@example.com',
    title: 'ms',
    firstName: 'Kitty',
    picture: 'https://randomuser.me/api/portraits/women/78.jpg',
    lastName: 'Steward',
  },
  {
    id: '8UfTdB7ctWt3Fl87d88Q',
    firstName: 'Vanessa',
    picture: 'https://randomuser.me/api/portraits/women/33.jpg',
    email: 'vanessa.ramos@example.com',
    lastName: 'Ramos',
    title: 'ms',
  },
  {
    id: '8YL1aG0vwRBXTzeZ0jRC',
    picture: 'https://randomuser.me/api/portraits/women/85.jpg',
    firstName: 'Olaí',
    email: 'olai.gomes@example.com',
    title: 'mrs',
    lastName: 'Gomes',
  },
  {
    id: '9N03J6vQj6MFq2UpUanW',
    email: 'constance.bourgeois@example.com',
    lastName: 'Bourgeois',
    firstName: 'Constance',
    title: 'miss',
    picture: 'https://randomuser.me/api/portraits/women/87.jpg',
  },
  {
    id: 'CNYttp1Jrgg3I2zfSeS4',
    email: 'kenneth.carter@example.com',
    picture: 'https://randomuser.me/api/portraits/men/40.jpg',
    lastName: 'Carter',
    firstName: 'Kenneth',
    title: 'mr',
  },
  {
    id: 'EiYwv4wPYXfKgEKyTUyN',
    title: 'mr',
    firstName: 'Sigmund',
    picture: 'https://randomuser.me/api/portraits/men/61.jpg',
    lastName: 'Myran',
    email: 'sigmund.myran@example.com',
  },
  {
    id: 'EvXSjRCdYryxUp3j1Akj',
    picture: 'https://randomuser.me/api/portraits/women/3.jpg',
    email: 'josefina.calvo@example.com',
    lastName: 'Calvo',
    title: 'mrs',
    firstName: 'Josefina',
  },
  {
    id: 'FLgxIsydJ28gBIIKZeMg',
    email: 'annabel.somby@example.com',
    title: 'ms',
    firstName: 'Annabel',
    picture: 'https://randomuser.me/api/portraits/women/35.jpg',
    lastName: 'Somby',
  },
  {
    id: 'FOxy7zUy2SiEN14mQazD',
    lastName: 'Brand',
    title: 'mr',
    firstName: 'Friedrich-Karl',
    picture: 'https://randomuser.me/api/portraits/men/7.jpg',
    email: 'friedrich-karl.brand@example.com',
  },
  {
    id: 'G19Ya7yxByl6bUXITXzT',
    email: 'sibylle.leibold@example.com',
    firstName: 'Sibylle',
    picture: 'https://randomuser.me/api/portraits/women/89.jpg',
    title: 'mrs',
    lastName: 'Leibold',
  },
];

const Item = ({ item }) => (
    <View style={styles.item}>
      <View style={styles.item_image}>
        <Image
            resizeMode={'cover'}
            source={{
              uri: item.picture,
            }}
            style={styles.image}
        />
      </View>
      <View style={styles.item_content}>
        <Text style={styles.item_content_text}>
          <Text style={styles.item_content_title_text}>Nombre: </Text>
          {item.title + ' ' + item.firstName + ' ' + item.lastName}
        </Text>
        <Text>
          <Text style={styles.item_content_title_text}>E-mail: </Text>
          {item.email}
        </Text>
      </View>
    </View>
);

function MyList() {
  const [usuarios, setUsuarios] = useState([]);

  const BASE_URL = 'https://dummyapi.io/data/apid';
  const APP_ID = '6069741bfe0a0f36017d0e16';

  useEffect(() => {
    fetch(BASE_URL + '/user', {
      method: 'GET', // or 'PUT'
      headers: { 'app-id': APP_ID },
      mode: 'cors',
    })
        .then((res) => res.json())
        .then((res) => {
          //actualizamos el estado con los datos
          setUsuarios(res.data);
        });
  }, []);

  const renderItem = ({ item }) => <Item item={item} />;

  return (
      <View style={styles.container}>
        <Text style={styles.title}>Usuarios</Text>
        <FlatList
            data={DATA}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
        />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    fontFamily: 'Helvetica',
  },
  title: {
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    fontSize: 25,
    fontWeight: "bold",
    color: '#008080',
  },
  textResult: {
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    fontSize: 20,
    color: 'red',
  },
  input: {
    height: 40,
    borderColor: '#fff',
    borderWidth: 1,
    backgroundColor: '#3CBC8D',
    borderRadius: 10,
    color: 'white',
    paddingLeft: 10,
    marginBottom: 30,
    fontSize: 20,
  },
  image: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  item: {
    height: 120,
    flex:1,
    flexDirection: "row",
    margin: 5,
    borderRadius: 10,
    borderWidth: 4,
    borderColor: '#008080',
    borderStyle: "solid",
    overflow:'hidden',

  },
  item_image:{
    flex: 0.4
  },
  item_content:{
    flex: 0.6,
    padding: 10,
  },
  item_content_text:{
    textTransform:'capitalize'
  },
  item_content_title_text:{
    color: '#675151',
    fontWeight: "bold",
  }
});

export default MyList;
